package com.github.diego7167.funds.recipe

import com.github.diego7167.funds.Funds
import net.minecraft.recipe.RecipeSerializer
import net.minecraft.recipe.RecipeType
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry

object RecipeRegistry {
    val coinPressingSerializer = registerSerializer("coin_pressing", CoinPressingSerializer)

    val coinPressingRecipeType = registerRecipeType("coin_pressing", CoinPressingRecipe.Type)

    fun init() {
        // Load class
    }

    private fun registerSerializer(id: String, serializer: RecipeSerializer<*>): RecipeSerializer<*> =
        Registry.register(Registry.RECIPE_SERIALIZER, Identifier(Funds.modID, id), serializer)

    private fun registerRecipeType(id: String, recipeType: RecipeType<*>): RecipeType<*> =
        Registry.register(Registry.RECIPE_TYPE, Identifier(Funds.modID, id), recipeType)
}