package com.github.diego7167.funds.recipe

import com.google.gson.JsonObject

class CoinPressingFormat(
    val base: JsonObject?,
    val result: String?
)