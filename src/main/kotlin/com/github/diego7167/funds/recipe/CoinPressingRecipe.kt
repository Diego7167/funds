package com.github.diego7167.funds.recipe

import com.github.diego7167.funds.Funds
import com.github.diego7167.funds.blocks.CoinPressInventory
import net.minecraft.item.ItemStack
import net.minecraft.recipe.*
import net.minecraft.util.Identifier
import net.minecraft.world.World

class CoinPressingRecipe(
    private val base: Ingredient,
    private val outputStack: ItemStack,
    private val recipeId: Identifier
): Recipe<CoinPressInventory> {
    fun getBase(): Ingredient = base

    override fun matches(inv: CoinPressInventory, world: World?): Boolean = base.test(inv.getStack(0))

    override fun craft(inv: CoinPressInventory?): ItemStack = ItemStack.EMPTY

    override fun fits(width: Int, height: Int): Boolean = false

    override fun getOutput(): ItemStack = outputStack

    override fun getId(): Identifier = recipeId

    override fun getSerializer(): RecipeSerializer<*> = CoinPressingSerializer

    override fun getType(): RecipeType<*> = Type

    object Type: RecipeType<CoinPressingRecipe> {
        val id = Identifier(Funds.modID, "coin_pressing")

        val name = Identifier(Funds.modID, "coin_press_recipe")
    }
}