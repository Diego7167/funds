package com.github.diego7167.funds.recipe

import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import net.minecraft.item.ItemStack
import net.minecraft.network.PacketByteBuf
import net.minecraft.recipe.Ingredient
import net.minecraft.recipe.RecipeSerializer
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry

object CoinPressingSerializer: RecipeSerializer<CoinPressingRecipe> {
    override fun read(id: Identifier, json: JsonObject): CoinPressingRecipe {
        val recipeJson = Gson().fromJson(json, CoinPressingFormat::class.java)
        if(recipeJson.base == null || recipeJson.result == null) { // If values are not present
            throw JsonSyntaxException("A required attribute is missing")
        }

        val base = Ingredient.fromJson(recipeJson.base)
        val resultItem = Registry.ITEM.getOrEmpty(Identifier(recipeJson.result))
            .orElseThrow(fun () = JsonSyntaxException("No such item: ${recipeJson.result}")) // Throw is item not found
        val result = ItemStack(resultItem, 1)

        return CoinPressingRecipe(base, result, id)
    }

    override fun read(id: Identifier, buf: PacketByteBuf): CoinPressingRecipe {
        val base = Ingredient.fromPacket(buf)
        val output = buf.readItemStack()
        return CoinPressingRecipe(base, output, id)
    }

    override fun write(buf: PacketByteBuf, recipe: CoinPressingRecipe) {
        recipe.getBase().write(buf)
        buf.writeItemStack(recipe.output)
    }
}