package com.github.diego7167.funds

import com.github.diego7167.funds.blocks.BlockRegistry
import com.github.diego7167.funds.blocks.CoinPressRenderer
import com.github.diego7167.funds.items.ItemRegistry
import com.github.diego7167.funds.recipe.RecipeRegistry
import net.fabricmc.fabric.api.client.rendereregistry.v1.BlockEntityRendererRegistry
import net.minecraft.util.Identifier

@Suppress("unused", "MemberVisibilityCanBePrivate")
object Funds {
    const val modID = "funds"

    fun initialize() {
        // Blocks
        BlockRegistry.init()

        // Items
        ItemRegistry.init()

        // Recipes
        RecipeRegistry.init()
    }

    fun initializeClient() {
        BlockEntityRendererRegistry.INSTANCE.register(
            BlockRegistry.coinPressEntity,
            ::CoinPressRenderer
        )
    }

    private fun identifier(id: String) = Identifier(modID, id)
}

