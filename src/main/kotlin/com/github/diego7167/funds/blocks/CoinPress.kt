package com.github.diego7167.funds.blocks

import com.github.diego7167.funds.Funds
import com.github.diego7167.funds.Utils
import net.minecraft.block.*
import net.minecraft.block.entity.BlockEntity
import net.minecraft.entity.ItemEntity
import net.minecraft.entity.LivingEntity
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.ItemStack
import net.minecraft.util.ActionResult
import net.minecraft.util.Hand
import net.minecraft.util.hit.BlockHitResult
import net.minecraft.util.math.BlockPos
import net.minecraft.util.shape.VoxelShape
import net.minecraft.util.shape.VoxelShapes
import net.minecraft.world.BlockView
import net.minecraft.world.World

class CoinPress(settings: Settings): Block(settings), BlockEntityProvider {
    override fun getOutlineShape(
        state: BlockState?,
        world: BlockView?,
        pos: BlockPos?,
        context: ShapeContext?
    ): VoxelShape {
        return VoxelShapes.cuboid(
            0.0,
            0.0,
            0.0,
            1.0,
            Utils.of16(14.0),
            1.0
        )
    }

    override fun createBlockEntity(world: BlockView?): BlockEntity {
        return CoinPressEntity()
    }

    override fun onUse(
        state: BlockState?,
        world: World?,
        pos: BlockPos?,
        player: PlayerEntity?,
        hand: Hand?,
        hit: BlockHitResult?
    ): ActionResult {
        if(world!!.isClient) return ActionResult.SUCCESS
        val entity = world.getBlockEntity(pos) as CoinPressEntity

        if(!player!!.getStackInHand(hand!!).isEmpty) {
            val stack = player.getStackInHand(hand)
            if(stack.item == Funds.pressPlate && entity.getStack(1).isEmpty) {
                entity.setStack(1, stack.split(1))
            }

            if(entity.getStack(0).isEmpty) {
                entity.setStack(0, stack.split(1))
            }
        } else {
            if(!entity.getStack(0).isEmpty) {
                val stack = entity.getStack(0)
                player.inventory.offerOrDrop(world, stack)
            } else if(!entity.getStack(1).isEmpty) {
                val stack = entity.getStack(1)
                player.inventory.offerOrDrop(world, stack)
            }
        }
        entity.doUpdate()

        return ActionResult.SUCCESS
    }

    override fun hasComparatorOutput(state: BlockState?): Boolean {
        return true
    }

    override fun getComparatorOutput(state: BlockState?, world: World, pos: BlockPos): Int {
        val entity = world.getBlockEntity(pos) as CoinPressEntity

        return if(entity.redstoneOutput) {
            15
        } else {
            0
        }
    }

    override fun onBreak(world: World, pos: BlockPos, state: BlockState?, player: PlayerEntity?) {
        val entity = world.getBlockEntity(pos) as CoinPressEntity
        for(stack in entity.getItems()) {
            world.spawnEntity(ItemEntity( // Drop contained item
                world,
                pos.x.toDouble() + 0.5,
                pos.y.toDouble() + 0.5,
                pos.z.toDouble() + 0.5,
                stack
            ))
        }

        super.onBreak(world, pos, state, player)
    }
}