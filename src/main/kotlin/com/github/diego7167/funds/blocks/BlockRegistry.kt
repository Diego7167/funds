package com.github.diego7167.funds.blocks

import com.github.diego7167.funds.Funds
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.block.Block
import net.minecraft.block.Material
import net.minecraft.block.entity.BlockEntity
import net.minecraft.block.entity.BlockEntityType
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import java.util.function.Supplier

object BlockRegistry {
    val coinPress = registerBlock("coin_press", CoinPress(FabricBlockSettings.of(Material.METAL).strength(4f)))

    val coinPressEntity = registerBlockEntity("coin_press", BlockEntityType.Builder.create(
        Supplier(::CoinPressEntity),
        coinPress
    ).build(null))

    fun init() {
        // Load class
    }

    private fun registerBlock(id: String, block: Block): Block =
        Registry.register(Registry.BLOCK, Identifier(Funds.modID, id), block)

    private fun <T: BlockEntity> registerBlockEntity(id: String, blockEntityType: BlockEntityType<T>): BlockEntityType<T> =
        Registry.register(Registry.BLOCK_ENTITY_TYPE, Identifier(Funds.modID, id), blockEntityType)
}