package com.github.diego7167.funds.blocks

import com.github.diego7167.funds.Funds
import com.github.diego7167.funds.recipe.CoinPressingRecipe
import net.fabricmc.fabric.api.block.entity.BlockEntityClientSerializable
import net.minecraft.block.BlockState
import net.minecraft.block.entity.BlockEntity
import net.minecraft.inventory.Inventories
import net.minecraft.item.ItemStack
import net.minecraft.nbt.CompoundTag
import net.minecraft.sound.SoundCategory
import net.minecraft.sound.SoundEvents
import net.minecraft.util.Tickable
import net.minecraft.util.collection.DefaultedList
import java.util.*

class CoinPressEntity:
    BlockEntity(Funds.coinPressEntity),
    BlockEntityClientSerializable,
    CoinPressInventory,
    Tickable
{
    private var items = DefaultedList.ofSize(2, ItemStack.EMPTY)
    private var powered = false
    private var ready = false
    var redstoneOutput = false

    override fun getItems(): DefaultedList<ItemStack> = items

    fun doUpdate() {
        world!!.updateComparators(pos, Funds.coinPress)
        this.sync()
    }
    override fun synchronize() = doUpdate()

    override fun canExtract() = ready

    private fun isInput(stack: ItemStack): Boolean {
        val recipes = world!!.recipeManager.listAllOfType(CoinPressingRecipe.Type)

        for(recipe in recipes) {
            if(recipe.getBase().test(stack)) {
                return true
            }
        }
        return false
    }
    private fun isOutput(stack: ItemStack): Boolean {
        val recipes = world!!.recipeManager
            .listAllOfType(CoinPressingRecipe.Type)

        for(recipe in recipes) {
            if(recipe.output.item == stack.item) {
                return true
            }
        }
        return false
    }

    override fun tick() {
        if(world!!.isClient) return

        val stack = this.getStack(0)
        if(isInput(stack)) {
            ready = false
            if (world!!.isReceivingRedstonePower(pos)) {
                if (!powered) {
                    powered = true
                    processItem()
                }
            } else {
                powered = false
            }
        } else {
            ready = isOutput(stack)
        }

        redstoneOutput = isInput(stack)

        doUpdate()
    }

    private fun processItem() {
        val plate = getStack(1)
        if(plate.isEmpty)
            return

        val match = world!!.recipeManager
            .getFirstMatch(CoinPressingRecipe.Type, this, world)

        val itemStack = match.get().output.copy()
        val tag = itemStack.orCreateTag
        tag.putString("creator", plate.tag?.getString("creator"))
        itemStack.toTag(tag)

        this.setStack(0, itemStack)
        world!!.playSound(
            null,
            pos,
            SoundEvents.BLOCK_PISTON_EXTEND,
            SoundCategory.BLOCKS,
            0.6f,
            1f
        )
        doUpdate()
    }

    override fun fromTag(state: BlockState, tag: CompoundTag) {
        super.fromTag(state, tag)

        powered = tag.getBoolean("powered")
        ready = tag.getBoolean("ready")
        redstoneOutput = tag.getBoolean("redstoneOutput")
        Inventories.fromTag(tag, items)
    }

    override fun toTag(compoundTag: CompoundTag): CompoundTag {
        val tag = super.toTag(compoundTag)
        tag.putBoolean("powered", powered)
        tag.putBoolean("ready", ready)
        tag.putBoolean("redstoneOutput", redstoneOutput)

        Inventories.toTag(tag, items)
        return tag
    }

    override fun fromClientTag(tag: CompoundTag?) {
        items.clear() // For when an item is removed
        Inventories.fromTag(tag, items)
    }

    override fun toClientTag(compoundTag: CompoundTag?): CompoundTag {
        val tag = super.toTag(compoundTag)

        Inventories.toTag(tag, items)
        return tag
    }
}