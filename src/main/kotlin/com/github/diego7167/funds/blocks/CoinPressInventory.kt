package com.github.diego7167.funds.blocks

import net.minecraft.entity.player.PlayerEntity
import net.minecraft.inventory.Inventories
import net.minecraft.inventory.Inventory
import net.minecraft.inventory.SidedInventory
import net.minecraft.item.ItemStack
import net.minecraft.util.collection.DefaultedList
import net.minecraft.util.math.Direction

interface CoinPressInventory: Inventory, SidedInventory {
    fun getItems(): DefaultedList<ItemStack>
    fun synchronize()
    fun canExtract(): Boolean

    override fun size(): Int {
        return getItems().size
    }

    override fun isEmpty(): Boolean {
        val stacks = getItems()
        for(stack in stacks) {
            if(stack.isEmpty)
                return false
        }
        return true
    }

    override fun getStack(slot: Int): ItemStack {
        return getItems()[slot]
    }

    override fun removeStack(slot: Int, count: Int): ItemStack {
        val result = Inventories.splitStack(getItems(), slot, count)
        if(!result.isEmpty) {
            markDirty()
        }
        synchronize()
        return result
    }

    override fun removeStack(slot: Int): ItemStack {
        val stack = Inventories.removeStack(getItems(), slot)
        synchronize()
        return stack
    }

    override fun setStack(slot: Int, stack: ItemStack?) {
        getItems()[slot] = stack!!
        if(stack.count > maxCountPerStack) {
            stack.count = maxCountPerStack
        }
        synchronize()
    }

    override fun clear() {
        getItems().clear()
        synchronize()
    }

    override fun canInsert(slot: Int, stack: ItemStack?, dir: Direction?): Boolean {
        return slot != 1 && getItems()[slot].isEmpty
    }

    override fun canExtract(slot: Int, stack: ItemStack?, dir: Direction?): Boolean {
        return canExtract()
    }

    override fun getAvailableSlots(side: Direction?): IntArray {
        return IntArray(1) {it} // Any side works
    }

    override fun canPlayerUse(player: PlayerEntity?): Boolean = true
}