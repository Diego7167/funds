package com.github.diego7167.funds.blocks

import com.github.diego7167.funds.Utils
import net.minecraft.client.MinecraftClient
import net.minecraft.client.render.VertexConsumerProvider
import net.minecraft.client.render.block.entity.BlockEntityRenderDispatcher
import net.minecraft.client.render.block.entity.BlockEntityRenderer
import net.minecraft.client.render.model.json.ModelTransformation
import net.minecraft.client.util.math.MatrixStack
import net.minecraft.client.util.math.Vector3f

class CoinPressRenderer(dispatcher: BlockEntityRenderDispatcher):
    BlockEntityRenderer<CoinPressEntity>(dispatcher)
{
    override fun render(
        entity: CoinPressEntity,
        tickDelta: Float,
        matrices: MatrixStack,
        vertexConsumers: VertexConsumerProvider,
        light: Int,
        overlay: Int
    ) {
        // Render item to be pressed
        val stack = entity.getStack(0)
        matrices.push()

        matrices.translate(0.5, Utils.of16(3.5), Utils.of16(10.0))
        matrices.multiply(Vector3f.NEGATIVE_X.getDegreesQuaternion(90f))

        MinecraftClient.getInstance().itemRenderer.renderItem(stack, ModelTransformation.Mode.GROUND, light, overlay, matrices, vertexConsumers)
        matrices.pop()

        // Render press plate
        val plate = entity.getStack(1)
        matrices.push()

        matrices.translate(0.5, Utils.of16(10.75), Utils.of16(10.0))
        matrices.multiply(Vector3f.NEGATIVE_X.getDegreesQuaternion(90f))

        MinecraftClient.getInstance().itemRenderer.renderItem(plate, ModelTransformation.Mode.GROUND, light, overlay, matrices, vertexConsumers)
        matrices.pop()
    }
}