package com.github.diego7167.funds.items

import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.util.Hand
import net.minecraft.util.TypedActionResult
import net.minecraft.world.World

class CoinLens(settings: Settings): Item(settings) {
    override fun use(world: World, user: PlayerEntity?, hand: Hand?): TypedActionResult<ItemStack> {
        val coin = user?.offHandStack ?: return TypedActionResult.fail(user?.getStackInHand(hand)) // Fail if offhand is null

        val tag = coin.tag?.get("creator") ?: return TypedActionResult.fail(user.getStackInHand(hand)) // Fail if offhand doesn't have creator tag

        if(world.isClient)
            user.sendMessage(tag.toText(), false)
        return TypedActionResult.success(user.getStackInHand(hand))
    }
}