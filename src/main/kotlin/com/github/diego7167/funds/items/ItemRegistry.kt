package com.github.diego7167.funds.items

import com.github.diego7167.funds.Funds
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder
import net.minecraft.item.Item
import net.minecraft.item.ItemGroup
import net.minecraft.item.ItemStack
import net.minecraft.util.Identifier
import net.minecraft.util.registry.Registry
import java.util.function.Supplier

object ItemRegistry {
    val fundsGroup: ItemGroup = FabricItemGroupBuilder.build(
        Identifier("funds", "general"),
        Supplier { return@Supplier ItemStack(ironCoin) }
    )

    val pressPlate = registerItem("press_plate", PressPlate(Item.Settings().group(fundsGroup).maxCount(1)))
    val coinLens = registerItem("coin_lens", CoinLens(Item.Settings().group(fundsGroup).maxCount(1)))
    val ironCoin = registerItem("iron_coin", Item(Item.Settings().group(fundsGroup)))
    val goldenCoin = registerItem("golden_coin", Item(Item.Settings().group(fundsGroup)))

    fun init() {
        // Loads object
    }

    private fun registerItem(id: String, item: Item): Item =
        Registry.register(Registry.ITEM, Identifier(Funds.modID, id), item)
}