package com.github.diego7167.funds.items

import com.github.diego7167.funds.Funds
import com.github.diego7167.funds.blocks.CoinPress
import net.fabricmc.fabric.api.`object`.builder.v1.block.FabricBlockSettings
import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.BlockItem
import net.minecraft.item.ItemStack
import net.minecraft.world.World

class CoinPressItem(
    itemSettings: Settings
): BlockItem(Funds.coinPress, itemSettings) {
    override fun onCraft(stack: ItemStack, world: World?, player: PlayerEntity?) {
        super.onCraft(stack, world, player) // By default does nothing
        if(player != null) {
            val tag = stack.orCreateTag
            tag.putString("creator", player.uuidAsString)
        }
    }
}