package com.github.diego7167.funds.items

import net.minecraft.entity.player.PlayerEntity
import net.minecraft.item.Item
import net.minecraft.item.ItemStack
import net.minecraft.world.World
import java.util.*

class PressPlate(
    settings: Settings
): Item(settings) {
    override fun onCraft(stack: ItemStack, world: World?, player: PlayerEntity?) {
        super.onCraft(stack, world, player)

        val tag = stack.orCreateTag
        if (player != null) {
            tag.putString("creator", player.uuidAsString)
        } else {
            tag.putString("creator", UUID.randomUUID().toString())
        }
    }
}