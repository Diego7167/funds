# Funds
This mod requires [Fabric Language Kotlin](https://www.curseforge.com/minecraft/mc-mods/fabric-language-kotlin)

## Features:
- Iron and Gold coins, stack with other coins by same maker
- Coin press, powered by redstone signal (recipe support)
- Coin lens for identifying the maker of a coin by uuid

### Plans
- Better textures (I'm not an artist)
- Pressing animation
- Some advanced form of storage (Maybe a vault)
- REI integration
