# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.2] - 2021-03-10
### Fixed
- Coin press hopper logic

## [0.1.1] - 2021-03-10
### Added
- Press plate

### Changed
- Coin press has a second inventory slot for the press plate
- Instead of using the uuid of the player who placed the press,
  it now uses the person who crafted the plate

## [0.1.0] - 2021-03-09
### Added
- Iron and gold coins
- Coin press
- Coin lens
